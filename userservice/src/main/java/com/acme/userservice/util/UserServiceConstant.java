package com.acme.userservice.util;

import java.io.Serializable;

public final class UserServiceConstant implements Serializable {

    public static final String AUTHENTICATION = "/authentication";
    public static final String INVALID_CREDENTIALS = "Invalid Credentials";
    public static final String USER = "/user";
    public static final String ACME = "/acme";
    public static final String RESOURCE_NOT_FOUND_EXCEPTION = "ResourceNotFoundException : {}";
    public static final String CUSTOM_EXCEPTION = "CustomException : {}";
    public static final String NO_DATA_FOUND_EXCEPTION = "NoDataFoundException : {}";
    public static final String DATA_INTEGRITY_VIOLATION_EXCEPTION = "DataIntegrityViolationException : {}";
    public static final String METHOD_ARGUMENT_NOT_VALID_EXCEPTION = "MethodArgumentNotValidException : {}";
    public static final String GENERIC_EXCEPTION = "GenericException : {}";
    public static final String THIS_OPERATION_IS_NOT_ALLOWED = "This Operation is not allowed";
    public static final String AUTHORIZATION = "Authorization";
    public static final String MTOKEN = "MToken ";
    public static final String EMAIL_IS_NOT_REGISTERED = "Email is not registered";
    public static final String ADDED_SUCCESSFULLY = "Added Successfully";
    public static final String UPDATED_SUCCESSFULLY = "Updated Successfully";
    public static final String DELETED_SUCCESS = "Deleted Success";
    public static final String DELETE_FAILED_NO_RECORDS_FOUND = "Delete Failed, No Records Found";
    public static final String AUTH_WHITELIST_VALUE1 = "/user/authentication";
    public static final String AUTH_WHITELIST_VALUE2 = "/v3/api-docs/**";
    public static final String AUTH_WHITELIST_VALUE3 = "/swagger-ui/**";
    public static final String AUTH_WHITELIST_VALUE4 = "/acme/user";
    public static final String VALID_FIELD_MISSING = "Valid Field Missing";


}
