package com.acme.userservice.service;

import com.acme.userservice.entity.User;
import com.acme.userservice.model.UserRequest;
import com.acme.userservice.repo.UserRep;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserAuthenticationServiceTest {
    static UserRequest userRequest = new UserRequest();
    static com.acme.userservice.entity.User user = new User();
    static UserDetails userDetails = new org.springframework.security.core.userdetails.User("34#Admin", "password", new ArrayList<>());
    @Mock
    UserRep userRep;
    @InjectMocks
    UserAuthenticationService userAuthenticationService;

    @BeforeEach
    void setUp() {
        user.setId(34);
        user.setName("sam");
        user.setEmail("sam@gmail.com");
        user.setPassword("password");
        user.setRole("Admin");


        userRequest.setUserEmail("sam@gmail.com");
        userRequest.setUserPassword("password");
        userRequest.setUsername("34#Admin");
        userRequest.setUserRole("Admin");

    }

    @Test
    void loadUserByUsername() {

        Mockito.when(userRep.findByEmail(userRequest.getUserEmail())).thenReturn(user);
        assertEquals(userDetails, userAuthenticationService.loadUserByUsername(userRequest.getUserEmail()));

    }

    @Test
    void loadUserByUserId() {

        Mockito.when(userRep.findById(34)).thenReturn(user);
        assertEquals(userDetails, userAuthenticationService.loadUserByUserId(34));

    }
}