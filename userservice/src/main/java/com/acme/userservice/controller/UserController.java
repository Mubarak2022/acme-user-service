package com.acme.userservice.controller;

import com.acme.userservice.model.RestResponse;
import com.acme.userservice.model.UserRequest;
import com.acme.userservice.service.UserService;
import com.acme.userservice.util.UserServiceConstant;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(UserServiceConstant.ACME)
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(UserServiceConstant.USER)
    public RestResponse getUserData(@RequestParam("email") String email) {

        return userService.getUserData(email);
    }

    @PostMapping(UserServiceConstant.USER)
    public RestResponse addUser(@Valid @RequestBody UserRequest userRequest) {

        return userService.addUser(userRequest);
    }

    @PutMapping(UserServiceConstant.USER)
    public RestResponse updateUser(@Valid @RequestBody UserRequest userRequest) {

        return userService.updateUser(userRequest);
    }

    @DeleteMapping(UserServiceConstant.USER)
    public String deleteUserData(@RequestParam("email") String email) {

        return userService.deleteUser(email);
    }
}
