package com.acme.userservice.controller;

import com.acme.userservice.model.JwtRequest;
import com.acme.userservice.model.JwtResponse;
import com.acme.userservice.service.UserAuthenticationService;
import com.acme.userservice.util.JWTUtility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserAuthenticationServiceControllerTest {


    static JwtRequest jwtRequest = new JwtRequest();

    static JwtResponse jwtResponse = new JwtResponse();
    static UserDetails userDetails = new User("sam", "12344", new ArrayList<>());
    static UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
    @Mock
    UserAuthenticationService userAuthenticationService;
    @Mock
    AuthenticationManager authenticationManager;
    @Mock
    JWTUtility jwtUtility;
    @InjectMocks
    UserAuthenticationServiceController userAuthenticationServiceController;

    @BeforeEach
    void setUp() {

        jwtRequest.setUsername("sam");
        jwtRequest.setPassword("12334");
        usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(),
                jwtRequest.getPassword());
        jwtResponse.setJwtToken("Jwt token wet364tgret34353246t");

    }

    @Test
    void authenticate() {
        Mockito.when(authenticationManager.authenticate(usernamePasswordAuthenticationToken)).thenReturn(usernamePasswordAuthenticationToken);
        Mockito.when(userAuthenticationService.loadUserByUsername(jwtRequest.getUsername())).thenReturn(userDetails);
        Mockito.when(jwtUtility.generateToken(userDetails)).thenReturn(jwtResponse.getJwtToken());
        assertEquals(jwtUtility.generateToken(userDetails), userAuthenticationServiceController.authenticate(jwtRequest).getJwtToken());
    }

}