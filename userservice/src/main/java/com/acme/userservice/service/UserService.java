package com.acme.userservice.service;

import com.acme.userservice.model.RestResponse;
import com.acme.userservice.model.UserRequest;


public interface UserService {

    public RestResponse getUserData(String email);

    public RestResponse addUser(UserRequest userRequest);

    public RestResponse updateUser(UserRequest userRequest);

    public String deleteUser(String email);

}
