package com.acme.userservice.exception;

import com.acme.userservice.model.RestResponse;
import com.acme.userservice.util.UserServiceConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RestResponseExceptionHandler {


    @ExceptionHandler
    public ResponseEntity<RestResponse> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        log.info(UserServiceConstant.RESOURCE_NOT_FOUND_EXCEPTION, resourceNotFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), resourceNotFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleCustomException(CustomException customException) {
        log.info(UserServiceConstant.CUSTOM_EXCEPTION, customException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), customException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleNoDataFoundException(NoDataFoundException noDataFoundException) {
        log.info(UserServiceConstant.NO_DATA_FOUND_EXCEPTION, noDataFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), noDataFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleDataIntegrityViolationException(DataIntegrityViolationException dataIntegrityViolationException) {
        log.info(UserServiceConstant.DATA_INTEGRITY_VIOLATION_EXCEPTION, dataIntegrityViolationException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.FORBIDDEN.value(), UserServiceConstant.THIS_OPERATION_IS_NOT_ALLOWED), new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
        log.info(UserServiceConstant.METHOD_ARGUMENT_NOT_VALID_EXCEPTION, methodArgumentNotValidException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.BAD_REQUEST.value(), UserServiceConstant.VALID_FIELD_MISSING), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<RestResponse> handleGenericException(Exception exception) {
        log.info(UserServiceConstant.GENERIC_EXCEPTION, exception);
        return new ResponseEntity<>(new RestResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getLocalizedMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
