package com.acme.userservice.controller;

import com.acme.userservice.model.RestResponse;
import com.acme.userservice.model.UserRequest;
import com.acme.userservice.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserControllerTest {

    static UserRequest userRequest = new UserRequest();
    static RestResponse restResponse = new RestResponse();
    @Mock
    UserServiceImpl userService;
    @InjectMocks
    UserController userController;

    @BeforeEach
    void setUp() {
        userRequest.setUserEmail("sam@gmail.com");
        userRequest.setUserPassword("password");
        userRequest.setUsername("sam");
        userRequest.setUserRole("Admin");

        restResponse.setCode(200);
        restResponse.setMessage("successful");

    }


    @Test
    void getUserData() {
        Mockito.when(userService.getUserData(userRequest.getUserEmail())).thenReturn(restResponse);
        assertEquals(restResponse, userController.getUserData(userRequest.getUserEmail()));

    }

    @Test
    void addUser() {
        Mockito.when(userService.addUser(userRequest)).thenReturn(restResponse);
        assertEquals(restResponse, userController.addUser(userRequest));

    }

    @Test
    void updateUser() {
        Mockito.when(userService.updateUser(userRequest)).thenReturn(restResponse);
        assertEquals(restResponse, userController.updateUser(userRequest));

    }

    @Test
    void deleteUserData() {
        Mockito.when(userService.deleteUser(userRequest.getUserEmail())).thenReturn(restResponse.getMessage());
        assertEquals(restResponse.getMessage(), userController.deleteUserData(userRequest.getUserEmail()));

    }
}