package com.acme.userservice.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    @NotNull
    private String username;
    @NotNull
    private String userEmail;
    @NotNull
    private String userRole;

}
