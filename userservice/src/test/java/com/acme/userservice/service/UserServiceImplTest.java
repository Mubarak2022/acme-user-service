package com.acme.userservice.service;

import com.acme.userservice.entity.User;
import com.acme.userservice.exception.NoDataFoundException;
import com.acme.userservice.model.RestResponse;
import com.acme.userservice.model.UserRequest;
import com.acme.userservice.repo.UserRep;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserServiceImplTest {

    static UserRequest userRequest = new UserRequest();
    static RestResponse restResponse = new RestResponse();
    static User user = new User();
    @Mock
    UserRep userRep;
    @Mock
    PasswordEncoder passwordEncoder;
    @InjectMocks
    UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        user.setName("sam");
        user.setEmail("sam@gmail.com");
        user.setPassword("password");
        user.setRole("Admin");

        userRequest.setUserEmail("sam@gmail.com");
        userRequest.setUserPassword("password");
        userRequest.setUsername("sam");
        userRequest.setUserRole("Admin");

        restResponse.setCode(200);
        restResponse.setMessage("successful");
    }


    @Test
    void getUserData() {

        Mockito.when(userRep.findByEmail(userRequest.getUserEmail())).thenReturn(user);
        assertEquals(restResponse.getCode(), userService.getUserData(userRequest.getUserEmail()).getCode());

    }

    @Test
    void addUser() {
        Mockito.when(passwordEncoder.encode(userRequest.getUserPassword())).thenReturn("password");
        Mockito.when(userRep.save(user)).thenReturn(user);
        assertEquals(restResponse.getCode(), userService.addUser(userRequest).getCode());

    }

    @Test
    void updateUser() throws NoDataFoundException {
        user.setId(34);
        Mockito.when(passwordEncoder.encode(userRequest.getUserPassword())).thenReturn("password");
        Mockito.when(userRep.findByEmail(userRequest.getUserEmail())).thenReturn(user);
        Mockito.when(userRep.save(user)).thenReturn(user);
        assertEquals(restResponse.getCode(), userService.updateUser(userRequest).getCode());

    }

    @Test
    void deleteUser() {
        Mockito.when(userRep.deleteByEmail(userRequest.getUserEmail())).thenReturn(1);
        assertEquals("Deleted Success", userService.deleteUser(userRequest.getUserEmail()));

    }
}