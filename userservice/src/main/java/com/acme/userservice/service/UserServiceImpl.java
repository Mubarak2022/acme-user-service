package com.acme.userservice.service;

import com.acme.userservice.entity.User;
import com.acme.userservice.exception.NoDataFoundException;
import com.acme.userservice.model.RestResponse;
import com.acme.userservice.model.UserRequest;
import com.acme.userservice.model.UserResponse;
import com.acme.userservice.repo.UserRep;
import com.acme.userservice.util.UserServiceConstant;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRep userRep;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRep userRep, PasswordEncoder passwordEncoder) {
        this.userRep = userRep;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public RestResponse getUserData(String email) {
        User user = userRep.findByEmail(email);
        if (user == null) {
            throw new NoDataFoundException(UserServiceConstant.EMAIL_IS_NOT_REGISTERED);
        }
        UserResponse userResponse = copyUserRequestBeans(user, new UserResponse());
        return new RestResponse(HttpStatus.OK.value(), userResponse.toString());
    }

    @Override
    public RestResponse addUser(UserRequest userRequest) {
        //Encrypt Password before saving
        userRequest.setUserPassword(passwordEncoder.encode(userRequest.getUserPassword()));
        User user = new User();
        userRep.save(copyUserRequestBeans(userRequest, user));
        return new RestResponse(HttpStatus.OK.value(), UserServiceConstant.ADDED_SUCCESSFULLY);
    }

    @Override
    public RestResponse updateUser(UserRequest userRequest) {
        //Encrypt Password before saving
        userRequest.setUserPassword(passwordEncoder.encode(userRequest.getUserPassword()));
        User user = userRep.findByEmail(userRequest.getUserEmail());
        if (user == null) {
            throw new NoDataFoundException(UserServiceConstant.EMAIL_IS_NOT_REGISTERED);
        }
        userRep.save(copyUserRequestBeans(userRequest, user));
        return new RestResponse(HttpStatus.OK.value(), UserServiceConstant.UPDATED_SUCCESSFULLY);
    }

    @Override
    public String deleteUser(String email) {
        return userRep.deleteByEmail(email) > 0 ? UserServiceConstant.DELETED_SUCCESS : UserServiceConstant.DELETE_FAILED_NO_RECORDS_FOUND;
    }

    private User copyUserRequestBeans(UserRequest userRequest, User user) {

        user.setName(userRequest.getUsername());
        user.setEmail(userRequest.getUserEmail());
        user.setPassword(userRequest.getUserPassword());
        user.setRole(userRequest.getUserRole().toUpperCase());

        return user;
    }

    private UserResponse copyUserRequestBeans(User user, UserResponse userResponse) {

        userResponse.setUsername(user.getName() != null ? user.getName() : userResponse.getUsername());
        userResponse.setUserEmail(user.getEmail());
        userResponse.setUserRole(user.getRole() != null ? user.getRole() : userResponse.getUserRole());

        return userResponse;
    }


}
