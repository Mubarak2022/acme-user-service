package com.acme.userservice.controller;

import com.acme.userservice.exception.CustomException;
import com.acme.userservice.model.JwtRequest;
import com.acme.userservice.model.JwtResponse;
import com.acme.userservice.service.UserAuthenticationService;
import com.acme.userservice.util.JWTUtility;
import com.acme.userservice.util.UserServiceConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserServiceConstant.USER)
@Slf4j
public class UserAuthenticationServiceController {

    final PasswordEncoder passwordEncoder;
    private final JWTUtility jwtUtility;
    private final AuthenticationManager authenticationManager;
    private final UserAuthenticationService userAuthenticationService;

    public UserAuthenticationServiceController(JWTUtility jwtUtility, AuthenticationManager authenticationManager, UserAuthenticationService userAuthenticationService, PasswordEncoder passwordEncoder) {
        this.jwtUtility = jwtUtility;
        this.authenticationManager = authenticationManager;
        this.userAuthenticationService = userAuthenticationService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(UserServiceConstant.AUTHENTICATION)
    public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws CustomException {
        try {

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            jwtRequest.getUsername(),
                            jwtRequest.getPassword()
                    )
            );

        } catch (BadCredentialsException e) {
            throw new CustomException(UserServiceConstant.INVALID_CREDENTIALS);
        }

        final UserDetails userDetails =
                userAuthenticationService.loadUserByUsername(jwtRequest.getUsername());

        final String token =
                jwtUtility.generateToken(userDetails);

        return new JwtResponse(token);
    }

}
