package com.acme.userservice.model;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestResponse {
    private Integer code;
    private String message;

}
